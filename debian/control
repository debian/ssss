Source: ssss
Maintainer: Tomasz Buchert <tomasz@debian.org>
Section: utils
Priority: optional
Build-Depends: debhelper (>= 11),
               debhelper-compat (= 12),
               libgmp3-dev,
               xmltoman
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/debian/ssss
Vcs-Git: https://salsa.debian.org/debian/ssss.git
Homepage: http://point-at-infinity.org/ssss/

Package: ssss
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Shamir's secret sharing scheme implementation
 Implementation of Shamir's Secret Sharing Scheme. The program suite
 does both: the generation of shares for a known secret, and the
 reconstruction of a secret using user-provided shares.
 .
 Shamir's Secret Sharing Scheme allows a secret to be split in to shares.
 These shares can then be distributed to different people. When the time comes
 to retrieve the secret then a preset number of the shares need to be combined.
 The number of shares created, and the number needed to retrieve the secret
 are set at splitting time. The number of shares required to re-create the
 secret can be chosen to be less that the number of shares created, so any
 large enough subset of the shares can retrieve the secret.
 .
 This scheme allows a secret to be shared, either to reduce the chances that
 the secret is lost, or to increase the number of parties that must cooperate
 to reveal the secret.
